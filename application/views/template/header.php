<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Selamat Datang di Compas Petualang</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/demo.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/hero.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap-select.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/awe-booking-font.css">

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/validator.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap-select.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
</head>
<body style="min-height:100%">
	<header id="header-page">
		<div class="container">
			<nav class="navbar navbar-default navbar-fixed-top awe-navbar">
			  <div class="container-fluid">
			    <!-- Brand and toggle get grouped for better mobile display -->
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand hidden-sm hidden-lg" href="#">Brand</a>
			    </div>

			    <!-- Collect the nav links, forms, and other content for toggling -->
			    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      <div class="col-md-5">
			      	<ul class="nav navbar-nav navbar-left">
			      		<li><a href="#">Layanan & Fasilitas</a></li>
				        <li><a href="#">Cara Pembayaran</a></li>
			      		<li><a href="#"><b>Diskon & Promosi</b></a></li>
				    </ul>
			      </div>
			      <div class="col-md-2">
			      	<a class="navbar-brand hidden-xs" href="<?php echo base_url(); ?>"><i class="fa fa-compass fa-4x" id="logos"></i></a>
			      </div>
			      <div class="col-md-5">
			      	<ul class="nav navbar-nav navbar-right">
			      		<li><a href="<?php echo site_url('gallery'); ?>"><b><i class="fa fa-camera fa-lg"></i> Galleri Foto</a></b></li>
				        <li><a href="<?php echo site_url('trip'); ?>"><b><i class="fa fa-plus-circle fa-lg"></i> Semua Trip</a></b></li>
				        <?php if ($this->session->userdata('is_login')) { ?>
				        	<li><a href="<?php echo site_url('pesanan'); ?>"><b><i class="fa fa-shopping-bag fa-lg"></i><small style="background:#f39c12;padding:4px;color:#fff;border-radius:50%;">3</small></a></b></li>
				        	<li class="dropdown">
					          <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><b><i class="fa fa-user fa-lg"></i></b></a>
					          <ul class="dropdown-menu dropdown-border">
					          	<li><a href="<?php echo site_url('dashboard'); ?>"><b style="font-size:0.85em;">Dashboard</b></a></li>
					            <li role="separator" class="divider"></li>
					            <li><a href="#">Ubah Profil</a></li>
					            <li><a href="#">Ubah Password</a></li>
					            <li role="separator" class="divider"></li>
					            <li><a href="<?php echo site_url('pesanan'); ?>"><b style="font-size:0.85em;">(1) Atur Pesanan</b></a></li>
					          </ul>
					        </li>
				        	<li class="border-primary"><a href="<?php echo site_url('user/logout'); ?>"><i class="fa fa-power-off fa-lg"></i></a></li>
				        <?php } else {?>
				        	
				        	<li class="border-warning"><a href="<?php echo site_url('user'); ?>">Login | Register</a></li>
				        
				        <?php } ?>
				        
				      </ul>
			      </div>
			    </div><!-- /.navbar-collapse -->
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</header>
	<br>