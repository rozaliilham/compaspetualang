
<section class="bg-img">
  <div class="container">
    <div class="col-md-8">
      
    </div>
    <div class="col-md-4 login">
      <h6>Sudah Punya Akun? <small>Silahkan Masuk</small></h6>

      <form role="form" action="<?php echo site_url('user/aksiLogin'); ?>" method="POST">
        <div class="form-group">
          <input type="text" class="form-control" id="username" name="uname" placeholder="Username">
        </div>
        <div class="form-group">
          <input type="password" class="form-control" id="pwd" name="pass" placeholder="Password">
        </div>
        <a href="#">Lupa Password?</a>
        <button type="submit" class="btn btn-warning btn-orange pull-right" name="submit">Login</button>
      </form>
    </div>
    <div class="col-md-4 informasi pull-right">
      <h6>Belum Bergabung? <small><a href="<?php echo site_url('user/register') ?>">Daftar Disini (Register)</a></small></h6>
    </div>
  </div>
</section>

