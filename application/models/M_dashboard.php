<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_dashboard extends CI_Model {
	function __construct(){
		$this->load->database();
	}
	function aksiLogin($data){		
		$d = $this->db->get_where('user',$data);	
		return $d->num_rows();
	}

	function aksiRegister($data){
		$field_user = array(
			'username' => $data['username'],
            'email' => $data['email'],
            'password' => $data['password'],
            'level' => 'user'
			);
		$query_user	= $this->db->insert('user', $field_user);

		if (!$query_user) {
			return NULL;
		}

		return $query_user;
	}

}