<section class="checkout">
	<hr>
	<div class="container" >
		<div class="col-md-10">
			<div class="col-md-12">
				<div class="info-checkout" style="background:rgba(236, 240, 241,0.4);padding:5px;margin-bottom:10px;">
					<h4><i class="fa fa-info-circle"></i> #3 PEMBAYARAN TRIP (1 Orang)</h4>
					<strong>Nama Trip : Menjangan Bali Spectacular (2 - 4 Desember 2016)</strong>
					<br>
					<strong>Biaya : 1 x IDR 2.000.000 /orang</strong>
					<hr>
					<h6><strong>Total Biaya : IDR 2.000.000</strong></h6>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-12">
					<h6>PILIH METODE PEMBAYARAN MENGGUNAKAN :</h6>
					<div class="form-group">
						<div class="input-group">
							<button type="button" class="btn btn-primary" style="border:none;"OnClick="location.href='<?php echo site_url('checkout/konfirmasipesanan'); ?>'">
								<img src="<?php echo base_url(); ?>assets/img/logomandiri.png" alt="">
								<strong>TRANSAKSI MELALUI ATM / BANK MANDIRI</strong>
							</button>
						</div>
					</div>
					<hr>
				</div>
				<div class="col-md-12">
					<button type="button" class="btn btn-default" OnClick="location.href='<?php echo site_url('checkout/data'); ?>'">Kembali</button>
					<hr>
				</div>
			</div>
		</div>
	</div>
</section>