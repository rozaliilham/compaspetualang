<section class="checkout">
	<hr>
	<div class="container" >
		<div class="col-md-12">

			<div class="col-md-12">
				<div class="container">
			    <div class="row">
			        <div class="col-md-10">
			            <div class="row">
			                <div class="col-xs-6 col-sm-6 col-md-6">
			                    <address>
			                        <strong>IDENTITAS PEMESAN</strong>
			                        <br>
			                        <i class="fa fa-user"></i> Ariq Cahya Wardhana | 33029281716
			                        <br>
			                        <i class="fa fa-phone-square"></i> 085732766022
			                    </address>
			                </div>
			            </div>
			            <div class="row">
			            	<hr>
			                <table class="table table-hover">
			                    <thead>
			                        <tr>
			                        	<th class="hidden-xs">#ID Order</th>
			                        	<th>Tanggal</th>
			                            <th>Nama Paket</th>
			                            <th>Status</th>
			                            <th class="hidden-xs">Pembayaran</th>
			                            <th class="text-center">Total Biaya</th>
			                            <th></th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                        <tr>
			                        	<td class="col-md-1 hidden-xs">34522677W</td>
			                        	<td class="col-md-1">12/11/2015</td>
			                            <td class="col-md-3"><em>Paket Wisata Menjangan Spectacular</em></h4></td>
			                            <td class="col-md-3"> Menunggu Konfirmasi Pembayaran <br><span class="bg-info"><a href="#">[Konfirmasi Pembayaran]</a></span></td>
			                            <td class="col-md-1 hidden-xs"> BANK MANDIRI</td>
			                            <td class="col-md-2 text-center">2.000.000</td>
			                            <td class="col-md-1"><a href="#">[Detail]</a></td>
			                        </tr>
			                        <tr>
			                        	<td class="col-md-1 hidden-xs">34522677W</td>
			                        	<td class="col-md-1">12/11/2015</td>
			                            <td class="col-md-3"><em>Paket Wisata Menjangan Spectacular</em></h4></td>
			                            <td class="col-md-3"><i class="fa fa-check-square"></i> Telah Terdaftar <br><span class="bg-warning"><a href="#"><i class="fa fa-print"></i> Cetak Kartu Trip</a></span></td>
			                            <td class="col-md-1 hidden-xs"> BANK MANDIRI</td>
			                            <td class="col-md-2 text-center">2.000.000</td>
			                            <td class="col-md-1"><a href="#"><i class="fa fa-print"></i> Invoice</a></td>
			                        </tr>
			                        <tr>
			                        	<td class="col-md-1 hidden-xs">34522677W</td>
			                        	<td class="col-md-1">12/11/2015</td>
			                            <td class="col-md-3"><em>Paket Wisata Menjangan Spectacular</em></h4></td>
			                            <td class="col-md-3"><i class="fa fa-check-square"></i> Telah Terdaftar <br><span class="bg-warning"><a href="#"><i class="fa fa-print"></i> Cetak Kartu Trip</a></span></td>
			                            <td class="col-md-1 hidden-xs"> BANK MANDIRI</td>
			                            <td class="col-md-2 text-center">2.000.000</td>
			                            <td class="col-md-1"><a href="#"><i class="fa fa-print"></i> Invoice</a></td>
			                        </tr>
			                    </tbody>
			                </table>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</section>