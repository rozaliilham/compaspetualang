/**
 * Author: Heather Corey
 * jQuery Simple Parallax Plugin
 *
 */
 
(function($) {
 
    $.fn.parallax = function(options) {
 
        var windowHeight = $(window).height();
 
        // Establish default settings
        var settings = $.extend({
            speed        : 0.15
        }, options);
 
        // Iterate over each object in collection
        return this.each( function() {
 
        	// Save a reference to the element
        	var $this = $(this);
 
        	// Set up Scroll Handler
        	$(document).scroll(function(){
 
    		        var scrollTop = $(window).scrollTop();
            	        var offset = $this.offset().top;
            	        var height = $this.outerHeight();
 
    		// Check if above or below viewport
			if (offset + height <= scrollTop || offset >= scrollTop + windowHeight) {
				return;
			}
 
			var yBgPosition = Math.round((offset - scrollTop) * settings.speed);
 
                 // Apply the Y Background Position to Set the Parallax Effect
    			$this.css('background-position', 'center ' + yBgPosition + 'px');
                
        	});
        });
    }
}(jQuery));

$('.bg-img','.bg-img2').parallax({
	speed :	0.25
});

$(document).ready(function(){
      $('body').append('<div id="toTop" class="btn btn-warning" style="border-radius:0px !important;background:rgba(243, 156, 18,1.0);"><i class="fa fa-caret-up fa-2x"></i></div>');
        $(window).scroll(function () {
            if ($(this).scrollTop() != 0) {
                $('#toTop').fadeIn();
            } else {
                $('#toTop').fadeOut();
            }
        }); 
    $('#toTop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });
});

$(document).ready(

  function() { 

    $("html").niceScroll({cursorwidth:"8px",cursorcolor:"rgba(0, 0, 0,0.4)", cursorborder:"transparent",cursorborderradius:"0px"});

  }

);


/* ------------ Instagram  ------------ */

var access_token = "18360510.5b9e1e6.de870cc4d5344ffeaae178542029e98b"; //*** YOU NEED TO GET YOUR OWN ACCESS TOKEN FROM INSTAGRAM
//http://instagram.com/developer/authentication/
//http://dmolsen.com/2013/04/05/generating-access-tokens-for-instagram/

var resolution = "standard_resolution"; // resolution: low_resolution, thumbnail, standard_resolution
var user_id = "1555935004"; //userid
var hashtag = "compaspetualang"; // #hashtag
var last_url = "";

//HASHTAG URL - USE THIS URL FOR HASHTAG PICS
//var start_url = "https://api.instagram.com/v1/tags/"+hashtag+"/media/recent/?access_token="+access_token;
//USER URL - USE THIS URL FOR USER PICS
var start_url = "https://api.instagram.com/v1/users/"+user_id+"/media/recent/?access_token="+access_token;

//https://api.instagram.com/v1/tags/racehungry/media/recent?access_token=1836…6303057241113856435_1395676110362&_=1395676128688&max_tag_id=1343521624608

function loadEmUp(next_url){

    //console.log("loadEmUp url:" + next_url);
    url = next_url;
    
    $(function() {
        $.ajax({
                type: "GET",
                dataType: "jsonp",
                cache: false,
                url: url ,
                success: function(data) {
                
                next_url = data.pagination.next_url;
                //count = data.data.length;
                //three rows of four
                count = 20; 
        
                //uncommment to see da codez
                //console.log("count: " + count );
                //console.log("next_url: " + next_url );
                //console.log("data: " + JSON.stringify(data) );
                
                for (var i = 0; i < count; i++) {
                        if (typeof data.data[i] !== 'undefined' ) {
                        //console.log("id: " + data.data[i].id);
                            $("#instagram").append("<div class='instagram-wrap' id='pic-"+ data.data[i].id +"' ><a target='_blank' href='" + data.data[i].link +"'><span class='likes'><i class='fa fa-heart'></i> "+data.data[i].likes.count +"</span><img class='instagram-image' src='" + data.data[i].images.low_resolution.url +"' /></a></div>"
                        );  
                        }  
                }     
                        
                console.log("next_url: " + next_url);
                $("#showMore").hide();
                if (typeof next_url === 'undefined' || next_url.length < 10 ) {
                    console.log("NO MORE");
                    $("#showMore").hide();
                    $( "#more" ).attr( "next_url", "");
                }
                
                
                else {
                    //set button value
                    console.log("displaying more");
                    $("#showMore").show();
                    $( "#more" ).attr( "next_url", next_url);
                    last_url = next_url;
                    
                }
            }
        });
    });
}

    


//CALL THE SCRIPT TO START...
$( document ).ready(function() {
    
    //APPEND LOAD MORE BUTTON TO THE BODY...
    $("#more" ).click(function() {  
        var next_url = $(this).attr('next_url');
        loadEmUp(next_url);
        return false;
    });

    //start your engines
    loadEmUp(start_url);

    
});