
<section class="push-down">
  <div class="container-fluid">
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <nav id="spy">
                <ul class="sidebar-nav nav">
                    <li class="sidebar-brand">
                        <a href="#"><i class="fa fa-home fa-lg"></i></a>
                    </li>
                    <li class="sidebar-brand">
                        <a href="#">
                          <i class="fa fa-user fa-lg"></i>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>

        <!-- Page content -->
        <div id="page-content-wrapper">
            <div class="content-header">
                <h2 id="home">
                    <a id="menu-toggle" href="#" class="btn-menu toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                </h2>
            </div>

            <div class="page-content inset" data-spy="scroll" data-target="#spy">
                <div class="row">
                  <ol class="breadcrumb">
                    <li><a href="#">Dashboard</a></li>
                    <li class="active"><a href="#">Home</a></li>
                  </ol>
                        <div class="jumbotron text-center" >
                            <h1>Compas Dashboard System!</h1>
                            <p>This is a sidebar navigation responsive template built off of Bootstrap 3.0 and simple sidebar template. It includes anchors, scroll spy, smooth scroll, and Awesome icon fonts.</p>
                            <p><a class="btn btn-default">Click On Me!</a>
                            <a class="btn btn-info">Tweet Me!</a></p>
                        </div>
    
                </div>
                
            </div>

        </div>

    </div>
    
  </div>
</section>
<script>
   
  /*Menu-toggle*/
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
    });

    /*Scroll Spy*/
    $('body').scrollspy({ target: '#spy', offset:80});

    /*Smooth link animation*/
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
</script>
