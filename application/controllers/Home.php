<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Home extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        //$this->load->model('home_model');
    }
 
    public function index()
    {
    	//$data['itinerary'] = $this->home_model->total_itinerary();
        //$data['destinasi'] = $this->home_model->total_destinasi();
        //$data['user'] = $this->home_model->total_user();
        $this->template->display('vhome');
    }

	/*function logout()
	{
		$this->session->sess_destroy();
		redirect('admin');
	} */   
}