<section class="checkout">
	<hr>
	<div class="container" >
		<div class="col-md-10">
			<div class="col-md-12">
				<div class="info-checkout" style="background:rgba(236, 240, 241,0.4);padding:5px;margin-bottom:10px;">
					<h4><i class="fa fa-info-circle"></i> #2 PENGISIAN ANGGOTA TRIP (1 Orang)</h4>
					<strong>Nama Trip : Menjangan Bali Spectacular (2 - 4 Desember 2016)</strong>
					<br>
					<strong>Biaya : 1 x IDR 2.000.000 /orang</strong>
					<hr>
					<h6><strong>Total Biaya : IDR 2.000.000</strong></h6>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-12">
					<h6>Informasi Anggota Trip</h6>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="sizing-addon1">1</span>
							<input type="text" class="form-control" placeholder="masukan nama anggota trip" aria-describedby="sizing-addon1">
							<input type="text" class="form-control" placeholder="nomor kartu identitas (KTP, SIM, atau Kartu Sekolah)" aria-describedby="sizing-addon1">
							<input type="text" class="form-control" placeholder="nomor handphone">
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
							<span class="input-group-addon" id="sizing-addon1">2</span>
							<input type="text" class="form-control" placeholder="masukan nama anggota trip" aria-describedby="sizing-addon1">
							<input type="text" class="form-control" placeholder="nomor kartu identitas (KTP, SIM, atau Kartu Sekolah)" aria-describedby="sizing-addon1">
							<input type="text" class="form-control" placeholder="nomor handphone">
						</div>
					</div>
					<hr>
				</div>
				<div class="col-md-12">
					<div class="btn-group btn-group-justified" role="group" aria-label="...">
					  <div class="btn-group" role="group">
					    <button type="button" class="btn btn-default" OnClick="location.href='<?php echo site_url('checkout'); ?>'">Kembali</button>
					  </div>
					  <div class="btn-group" role="group">
					    <button type="button" class="btn btn-primary" OnClick="location.href='<?php echo site_url('checkout/pembayaran'); ?>'"><strong><i class="fa fa-money"></i> Pembayaran</strong></button>
					  </div>
					</div>
					<hr>
				</div>
			</div>
		</div>
	</div>
</section>