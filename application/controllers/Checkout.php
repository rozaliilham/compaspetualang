<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Checkout extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        //$this->load->model('m_user');
    }
 
    public function index()
    {
    	$this->template->display('vcheckout');
    }

    function data(){
        $this->template->display('vcheckout2');
    }

    function pembayaran(){
        $this->template->display('vcheckout3');
    }

    function konfirmasipesanan(){
        $this->template->display('vcheckout4');
    }

    function infopesanan(){
        $this->template->display('vcheckout5');
    }

}