<section class="checkout">
	<hr>
	<div class="container" >
		<div class="col-md-10">
			<div class="col-md-12">
				<div class="info-checkout" style="background:rgba(236, 240, 241,0.4);padding:5px;margin-bottom:10px;">
					<h4><i class="fa fa-info-circle"></i> #4 KONFIRMASI PESANAN TRIP (1 Orang)</h4>
					<strong>Nama Trip : Menjangan Bali Spectacular (2 - 4 Desember 2016)</strong>
					<br>
					<strong>Biaya : 1 x IDR 2.000.000 /orang</strong>
					<hr>
					<h6><strong>Total Biaya : IDR 2.000.000</strong></h6>
				</div>
			</div>

			<div class="col-md-12">
				<div class="container">
			    <div class="row">
			        <div class="well col-md-9">
			            <div class="row">
			                <div class="col-xs-6 col-sm-6 col-md-6">
			                    <address>
			                        <strong>IDENTITAS PEMESAN</strong>
			                        <br>
			                        <i class="fa fa-user"></i> Ariq Cahya Wardhana | 33029281716
			                        <br>
			                        <i class="fa fa-phone-square"></i> 085732766022
			                    </address>
			                </div>
			                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
			                    <p>
			                        <em>Date: 1st November, 2015</em>
			                    </p>
			                    <p>
			                        <em>ORDER ID #: <strong>34522677W</strong></em>
			                    </p>
			                </div>
			            </div>
			            <div class="row">
			                <div class="text-center">
			                    <h6>Pembayaran Melalui Transfer ATM / BANK MANDIRI</h6>
			                </div>
			                </span>
			                <table class="table table-hover">
			                    <thead>
			                        <tr>
			                            <th>Product</th>
			                            <th>#</th>
			                            <th class="text-center">Price</th>
			                            <th class="text-center">Total</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                        <tr>
			                            <td class="col-md-9"><em>Baked Rodopa Sheep Feta</em></h4></td>
			                            <td class="col-md-1" style="text-align: center"> 2 </td>
			                            <td class="col-md-1 text-center">$13</td>
			                            <td class="col-md-1 text-center">$26</td>
			                        </tr>
			                        <tr>
			                            <td class="col-md-9"><em>Lebanese Cabbage Salad</em></h4></td>
			                            <td class="col-md-1" style="text-align: center"> 1 </td>
			                            <td class="col-md-1 text-center">$8</td>
			                            <td class="col-md-1 text-center">$8</td>
			                        </tr>
			                        <tr>
			                            <td class="col-md-9"><em>Baked Tart with Thyme and Garlic</em></h4></td>
			                            <td class="col-md-1" style="text-align: center"> 3 </td>
			                            <td class="col-md-1 text-center">$16</td>
			                            <td class="col-md-1 text-center">$48</td>
			                        </tr>
			                        <tr>
			                            <td>   </td>
			                            <td>   </td>
			                            <td class="text-right">
			                            <p>
			                                <strong>Subtotal: </strong>
			                            </p>
			                            <p>
			                                <strong>Tax: </strong>
			                            </p></td>
			                            <td class="text-center">
			                            <p>
			                                <strong>$6.94</strong>
			                            </p>
			                            <p>
			                                <strong>$6.94</strong>
			                            </p></td>
			                        </tr>
			                        <tr>
			                            <td>   </td>
			                            <td>   </td>
			                            <td class="text-right"><h4><strong>Total: </strong></h4></td>
			                            <td class="text-center text-danger"><h4><strong>$31.53</strong></h4></td>
			                        </tr>
			                    </tbody>
			                </table>
			                <button type="button" class="btn btn-success btn-lg btn-block" OnClick="location.href='<?php echo site_url('checkout/infopesanan'); ?>'">
			                    (Setuju) Konfirmasi Pesanan   <span class="glyphicon glyphicon-chevron-right"></span>
			                </button></td>
			            </div>
			        </div>
			    </div>
			    <hr>
				<div class="col-md-12">
					<button type="button" class="btn btn-default" OnClick="location.href='<?php echo site_url('checkout/pembayaran'); ?>'">Kembali</button>
					<hr>
				</div>
			</div>
		</div>
	</div>
</section>