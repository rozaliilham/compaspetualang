<style>


/*this is where jQuery appends the instagram json response*/
#instagram {
    float: left;
}

/*around each image*/
.instagram-wrap {
    float: left;
    position: relative;
    background: white;
    margin: 10px;
    padding: 5px;
    /*box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);*/
}

.instagram-wrap .likes {
    height: 16px;
    position: absolute;
    left: 10px;
    top: 10px;
    padding: 0 5px 0 5px;
    line-height: 16px;
    background: white no-repeat 2px 0;
    opacity: 0.6;
}

/*wrapper for more pics*/
#showMore{
    background: rgba(243, 156, 18,1.0);
    text-align: center;
    font-size: 1.4em;
}

/*button for more pics if available*/
#more {
padding: 10px;
color: #fff;
display: block;
}

</style>

<div class="container">
    <!-- instagram pics -->
    <div id="instagram"></div>
    <div class='clearfix'></div>
    <!-- button -->
    <div id="showMore">
        <div class='clearfix '><a id='more' next_url='"+next_url+"' href='#'>[Load More]</a></div>      
    </div>
</div>
