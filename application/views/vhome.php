<!-- Hero -->
	<section class="cd-hero">
        <ul class="cd-hero-slider autoplay">
            <li class="selected">
                <div class="cd-full-width">
                    <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true" style="margin-left:30%;"></div>
                    <h2><strong>COMPAS PETUALANG</strong></h2>
                    <h4 class="hidden-xs">ANDA MAU LIBURAN? compasin aja..</h4>
                    <p>Dapatkan penawaran harga murah untuk merasakan pengalaman liburan paling mengesankan dalam hidup anda.</p>
                    <a href="#" class="cd-btn"><i class="fa fa-user-plus fa-lg"></i> Join Us</a>
                </div> <!-- .cd-full-width -->
                </li>


                <li class="cd-bg-video">
                    <div class="cd-full-width">
                        <h2>Selamat Datang di Travary</h2>
                        <p>Cari Inspirasi Itinerary Perjalanan Backpacker di Indonesia</p>
                        <a href="#" class="cd-btn"><i class="fa fa-youtube-play"></i> Cara Menggunakan</a>
                    </div> <!-- .cd-full-width -->

                    <div class="cd-bg-video-wrapper" data-video="assets/video/video">
                        <!-- video element will be loaded using jQuery -->
                    </div> <!-- .cd-bg-video-wrapper -->
                </li>
            </ul> <!-- .cd-hero-slider -->

    </section> <!-- .cd-hero -->

    <!-- Promo 1 -->
    <section class="promo1" align="center">
        <h5>100+ klien puas liburan bareng Compas Petualang</h5>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                        <div class="item">
                            <div class="row">
                                <div class="col-md-2"><a href="#" class="thumbnail"><img src="https://www.sribulancer.com/assets/home/v2/client-sribulancer-1-2f80d4f1bdb398a5298ea37313ada333.png" alt="Image" style="max-width:100%;border:none !important;"></a></div>
                                <div class="col-md-2"><a href="#" class="thumbnail"><img src="https://www.sribulancer.com/assets/home/v2/client-sribulancer-2-768d55dbf7de357c7d3f8f9681aa7f6c.png" alt="Image" style="max-width:100%;"></a></div>
                                <div class="col-md-2"><a href="#" class="thumbnail"><img src="https://www.sribulancer.com/assets/home/v2/client-sribulancer-3-f900d2f328e481a64ca2dbcbfada176c.png" alt="Image" style="max-width:100%;"></a></div>
                                <div class="col-md-2"><a href="#" class="thumbnail"><img src="https://www.sribulancer.com/assets/home/v2/client-sribulancer-4-3de38f263405c59d28e750324a82a39d.png" alt="Image" style="max-width:100%;"></a></div>
                                <div class="col-md-2"><a href="#" class="thumbnail"><img src="https://www.sribulancer.com/assets/home/v2/client-sribulancer-5-08990989992371e9b5dddf5fde37566e.png" alt="Image" style="max-width:100%;"></a></div>
                                <div class="col-md-2"><a href="#" class="thumbnail"><img src="https://www.sribulancer.com/assets/home/v2/client-sribulancer-6-fbe43e16fe75e29104cde27e95c4ebce.png" alt="Image" style="max-width:100%;"></a></div>
                            </div><!--.row-->
                        </div><!--.item-->
                </div>
            </div>
        </div>
    </section>

    <!-- /Promo 1 -->
    <!-- Promo 2 -->
    <section class="promo2" align="center">
        <h4>Destinasi Populer</h4>
        <p><strong>Jelajahi tempat-tempat baru yang menginspirasi di Indonesia.</strong></p>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                        <div class="item">
                            <div class="row">
                                <div class="col-md-4">
                                    <a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/assets/img/candiborobudur.png" alt="Image" ></a>
                                </div>
                                <div class="col-md-4">
                                    <a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/assets/img/pantaikuta.png" alt="Image" ></a>
                                    <a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/assets/img/kawahijen.png" alt="Image" ></a>
                                </div>
                                <div class="col-md-4">
                                    <a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/assets/img/bromo.png" alt="Image" ></a>
                                    <a href="#" class="thumbnail"><img src="<?php echo base_url(); ?>/assets/img/menjangan.png" alt="Image" ></a>
                                </div>
                            </div><!--.row-->
                        </div><!--.item-->
                </div>
            </div>
            <button type="button" class="btn btn-warning tipe1">Lihat Seluruh Destinasi</button>
        </div>
    </section>

    <!-- /Promo 2 -->
    <!-- Trip Terbaru -->
    <section class="tripbaru">
        <div class="col-md-12">
            <div class="list-trip">
                    <a href="#">Open Trip Terbaru</a>
            </div>
                    <div id="trip-list">
                        <div class="trip-item">
                            <div class="row">
                                <div class="col-md-4 item-media">
                                    <div class="img-responsive">
                                            <img src="<?php echo base_url(); ?>/assets/img/3.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-5 item-body">
                                    <div class="item-title">
                                        <h5>
                                            <a href="#">Menjangan Bali Spectacular</a>
                                        </h5>
                                    </div>
                                    <div class="item-list">
                                        <ul>
                                            <li>4 Atraksi (Diving, Surfing, Barbeque)</li>
                                            <li>2 hari, 1 malam</li>
                                            <li>Menjangan, Kuta, Tanah Lot</li>
                                        </ul>
                                    </div>
                                    <div class="item-footer">
                                        <hr>
                                        <div class="item-icon">
                                            <strong>Fasilitas Gratis : </strong>
                                            <i class="awe-icon awe-icon-gym"></i>
                                            <i class="awe-icon awe-icon-car"></i>
                                            <i class="awe-icon awe-icon-food"></i>
                                            <i class="awe-icon awe-icon-level"></i>
                                            <i class="awe-icon awe-icon-wifi"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 item-price-booking">
                                    <div class="price">
                                        <b><i>2 - 4 Desember 2015</i></b>
                                        <h4>IDR 2.000.000 <small>/org</small></h4>
                                    </div>
                                    <a href="#s" class="awe-btn" role="button">Pesan Sekarang <i class="fa fa-angle-double-right fa-lg"></i></a>
                                </div>
                            </div>
                                
                        </div>
                        <div class="trip-item">
                            <div class="row">
                                <div class="col-md-4 item-media">
                                    <div class="img-responsive">
                                            <img src="<?php echo base_url(); ?>/assets/img/3.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-5 item-body">
                                    <div class="item-title">
                                        <h5>
                                            <a href="#">Menjangan Bali Spectacular</a>
                                        </h5>
                                    </div>
                                    <div class="item-list">
                                        <ul>
                                            <li>4 Atraksi (Diving, Surfing, Barbeque)</li>
                                            <li>2 hari, 1 malam</li>
                                            <li>Menjangan, Kuta, Tanah Lot</li>
                                        </ul>
                                    </div>
                                    <div class="item-footer">
                                        <hr>
                                        <div class="item-icon">
                                            <strong>Fasilitas Gratis : </strong>
                                            <i class="awe-icon awe-icon-gym"></i>
                                            <i class="awe-icon awe-icon-car"></i>
                                            <i class="awe-icon awe-icon-food"></i>
                                            <i class="awe-icon awe-icon-level"></i>
                                            <i class="awe-icon awe-icon-wifi"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 item-price-booking">
                                    <div class="price">
                                        <b><i>2 - 4 Desember 2015</i></b>
                                        <h4>IDR 2.000.000 <small>/org</small></h4>
                                    </div>
                                    <a href="#s" class="awe-btn" role="button">Pesan Sekarang <i class="fa fa-angle-double-right fa-lg"></i></a>
                                </div>
                            </div>
                                
                        </div>
                    <!-- End Of Trip Item -->  
                    </div>
                <div class="col-md-12" align="center" style="margin-top:10px;">
                    <button type="button" class="btn btn-warning tipe1">Lihat Semua Trip</button>
                </div>
        </div>
        
    </section>

    <!-- /Trip Terbaru -->

    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.5&appId=553083921503138";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>