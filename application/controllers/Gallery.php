<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Gallery extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        //$this->load->model('m_user');
    }
 
    public function index()
    {
    	$this->template->display('vgallery');
    }

}