<section class="checkout">
	<hr>
	<div class="container" >
		<div class="col-md-10">

			<div class="col-md-12">
				<div class="container">
			    <div class="row">
			        <div class="well col-md-10">
			            <div class="row">
			                <div class="col-xs-6 col-sm-6 col-md-6">
			                    <address>
			                        <strong>IDENTITAS PEMESAN</strong>
			                        <br>
			                        <i class="fa fa-user"></i> Ariq Cahya Wardhana | 33029281716
			                        <br>
			                        <i class="fa fa-phone-square"></i> 085732766022
			                    </address>
			                </div>
			                <div class="col-xs-6 col-sm-6 col-md-6 text-right">
			                    <p>
			                        <em>Date: 1st November, 2015</em>
			                    </p>
			                    <p>
			                        <em>ORDER ID #: <strong>34522677W</strong></em>
			                    </p>
			                </div>
			            </div>
			            <div class="row">
			            	<div class="alert alert-success" role="alert">
			            		<strong>Pesanan <abbr title="">ID-ORDER : 34522677W</abbr> Berhasil !</strong> <br>Terima Kasih Telah Melakukan Pesanan.. 
			            		
			            		Waktu pembayaran order <abbr title="Silahkan Lakukan Pembayaran Sesuai Dengan Metode Pembayaran Yang Dipilih">(Max. Selama 1x24 jam)</abbr>. <a href="#">[ Lihat Cara Pembayaran ]</a> 
			            		
			            	</div>
			            	<div class="alert alert-info" role="alert">
			            		Silahkan Melakukan Transaksi Ke Rekening Bank MANDIRI Compas Petualang Berikut Ini:
			            		<hr><strong>BANK : BANK MANDIRI CABANG SURABAYA</strong>
			            		<br><strong>NO. REKENING : 299872728191</strong>
			            		<br><strong>NAMA PEMILIK REKENING : HARITS AMRULLAH</strong>
			            	</div>
			                <table class="table table-hover">
			                    <thead>
			                        <tr>
			                            <th>Order</th>
			                            <th>Status</th>
			                            <th>Pembayaran</th>
			                            <th class="text-center">Biaya</th>
			                            <th class="text-center">Total</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                        <tr>
			                            <td class="col-md-3"><em>Paket Wisata Menjangan Spectacular</em></h4></td>
			                            <td class="col-md-3"> Menunggu Konfirmasi Pembayaran <br><span class="bg-info"><a href="#">[Konfirmasi Pembayaran]</a></span></td>
			                            <td class="col-md-2"> BANK MANDIRI</td>
			                            <td class="col-md-1 text-center">2.000.000</td>
			                            <td class="col-md-3 text-center">2.000.000</td>
			                        </tr>
			                        <tr>
			                        	<td>	</td>
			                            <td>   	</td>
			                            <td>   	</td>
			                            <td class="text-right">
			                            <p>
			                                <strong>Subtotal: </strong>
			                            </p>
			                            <p>
			                                <strong>Tax: </strong>
			                            </p></td>
			                            <td class="text-center">
			                            <p>
			                                <strong>2.000.000</strong>
			                            </p>
			                            <p>
			                                <strong>200.000</strong>
			                            </p></td>
			                        </tr>
			                        <tr>
			                        	<td>	</td>
			                            <td>   	</td>
			                            <td>   	</td>
			                            <td class="text-right"><h5><strong>Total: </strong></h5></td>
			                            <td class="text-center text-danger"><h5><strong>IDR 2.200.000,-</strong></h5></td>
			                        </tr>
			                    </tbody>
			                </table>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</section>