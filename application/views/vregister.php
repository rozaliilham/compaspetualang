
<section class="bg-img2">
  <div class="container">
    <div class="col-md-8">
      
    </div>
    <div class="col-md-4 register" align="justify">
      <h6>Pendaftaran Akun Baru <small>Gratis!</small></h6>

      <form role="form" action="<?php echo site_url('user/aksiRegister'); ?>" data-toggle="validator" method="POST">
        <div class="form-group">
          <input type="text" class="form-control" id="username" name="uname" placeholder="Username" required>
        </div>
        <div class="form-group">
          <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
        </div>
        <div class="form-group">
          <div class="form-group">
            <input name="pass" type="password" data-minlength="6" class="form-control" id="inputPassword" placeholder="Password (Minimal 6 karakter)" required>
            <span class="help-block"></span>
          </div>
          <div class="form-group">
            <input name="confirm-pass" type="password" class="form-control" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Whoops, these don't match" placeholder="Konfirmasi Password" required>
            <div class="help-block with-errors"></div>
          </div>
        </div>
        
        <a href="#"><small>Dengan Melakukan Pendaftaran Berarti Anda Telah Menyetujui Syarat & Ketentuan Yang Berlaku.</small></a>
        <hr>
        <button type="submit" class="btn btn-warning btn-orange pull-right" name="submit">Register</button>
      </form>
    </div>
    <div class="col-md-4 informasi pull-right">
      <h6>Sudah Bergabung? <small><a href="<?php echo site_url('user'); ?>">Masuk(Login)</a></small></h6>
    </div>
  </div>
</section>

