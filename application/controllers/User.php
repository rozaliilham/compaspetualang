<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class User extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->model('m_user');
    }
 
    public function index()
    {
    	//$data['itinerary'] = $this->home_model->total_itinerary();
        //$data['destinasi'] = $this->home_model->total_destinasi();
        //$data['user'] = $this->home_model->total_user();
        
        if ($this->session->userdata('is_login') == 1) {
        	/*redirect previous page */
			redirect($this->agent->referrer());
        }else{
        	$this->template->display('vlogin');
        }
    }

    function aksiLogin(){
    	$data=array(
			'username'=>$this->input->post('uname'),
			'password'=>$this->input->post('pass')
		);
			
		$cek=$this->m_user->aksiLogin($data);

		if($cek == 1){
			$x=$this->session->set_userdata($data);	
			$this->session->set_userdata('is_login','1');		
			/*redirect('login/sukses');*/
			redirect('home');
		}else{
			/*redirect previous page */
			redirect($this->agent->referrer());
		}
    }
    function logout(){		
		$this->session->sess_destroy();
		redirect('user');
	}

	function register(){
		if ($this->session->userdata('is_login') == 1) {
        	/*redirect previous page */
			redirect($this->agent->referrer());
        }else{
        	$this->template->display('vregister');
        }
	}

	function aksiRegister(){
		//if save button was clicked, get the data sent via post
        if ($this->input->server('REQUEST_METHOD') === 'POST')
        {
            	
                $data_to_store = array(                   
                    'username' => $this->input->post('uname'),
                    'email' => $this->input->post('email'),
                    'password' => $this->input->post('pass')
                );
                //if the insert has returned true then we show the flash message
                $CekDaftar= $this->m_user->aksiRegister($data_to_store);

                redirect('user/sukses', 'refresh');

        }
	}
	function sukses(){
		$this->template->display('vsukses');
		$this->output->set_header('refresh:3; url='.site_url("user"));
	}
}