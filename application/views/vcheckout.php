<section class="checkout">
	<hr>
	<div class="container" >
		<div class="col-md-10">
			<div class="col-md-12">
				<div class="info-checkout" style="background:rgba(236, 240, 241,0.4);padding:5px;margin-bottom:10px;">
					<h4><i class="fa fa-info-circle"></i> #1 PEMESANAN TRIP (1 Orang)</h4>
					<strong>Nama Trip 	: Menjangan Bali Spectacular (2 - 4 Desember 2016)</strong>
					<br>
					<strong>Biaya		: 1 x IDR 2.000.000 /orang</strong>
					<hr>
					<h6><strong>Total Biaya : IDR 2.000.000</strong></h6>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-7">
					<strong><i class="fa fa-exclamation-triangle"></i> Informasi Kontak Yang Dapat Dihubungi</strong>
					<div class="form-group">
						<input type="text" class="form-control" id="name" name="name" placeholder="Name" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="email" name="email" placeholder="Email" required>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" required>
					</div>
					<hr>
				</div>
				<div class="col-md-5">
					<br>
					<p>Jika Anda telah memiliki akun, Anda dapat melakukan login untuk mempercepat proses ini.</p>
					<button type="button" class="btn btn-warning" data-toggle="modal" data-target="#login">
					  Login
					</button>
				</div>
				<div class="col-md-12">
					<h6>Informasi Pesanan</h6>
					<div class="form-group">
						<div class="input-group">
						  <span class="input-group-addon" id="sizing-addon1">Nama Instansi</span>
						  <input type="text" class="form-control" placeholder="silahkan masukan nama instansi (Kosongkan jika pribadi)" aria-describedby="sizing-addon1">
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
						  	<span class="input-group-addon">Berapa Orang Yang Mengikuti Trip?</span>
						  	<select class="selectpicker dropup" data-style="btn-warning">
						      	<option>1</option>
						      	<option>2</option>
						      	<option>3</option>
						  	</select>
						</div>
					</div>
					<hr>
				</div>
				<div class="col-md-12">
					<div class="btn-group btn-group-justified" role="group" aria-label="...">
					  <div class="btn-group" role="group">
					    <button type="button" class="btn btn-default" disabled>Kembali</button>
					  </div>
					  <div class="btn-group" role="group">
					    <button type="button" class="btn btn-primary" OnClick="location.href='<?php echo site_url('checkout/data'); ?>'"><strong>Selanjutnya</strong></button>
					  </div>
					</div>
					<hr>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
              <h6 class="modal-title" id="myModalLabel">Login</h6>
          </div>
          <div class="modal-body">
              <div class="row">
                  <div class="col-xs-6">
                      <div class="well">
                          <form id="loginForm" method="POST" action="/login/" novalidate="novalidate">
                              <div class="form-group">
                                  <label for="username" class="control-label">Username</label>
                                  <input type="text" class="form-control" id="username" name="username" value="" required="" title="Please enter you username" placeholder="example@gmail.com">
                                  <span class="help-block"></span>
                              </div>
                              <div class="form-group">
                                  <label for="password" class="control-label">Password</label>
                                  <input type="password" class="form-control" id="password" name="password" value="" required="" title="Please enter your password">
                                  <span class="help-block"></span>
                              </div>
                              <div id="loginErrorMsg" class="alert alert-error hide">Wrong username og password</div>
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="remember" id="remember"> Remember login
                                  </label>
                                  <p class="help-block">(if this is a private computer)</p>
                              </div>
                              <button type="submit" class="btn btn-success btn-block">Login</button>
                              <a href="/forgot/" class="btn btn-default btn-block">Help to login</a>
                          </form>
                      </div>
                  </div>
                  <div class="col-xs-6">
                      <p class="lead">Register now for <span class="text-success">FREE</span></p>
                      <ul class="list-unstyled" style="line-height: 2">
                          <li><span class="fa fa-check text-success"></span> See all your orders</li>
                          <li><span class="fa fa-check text-success"></span> Fast re-order</li>
                          <li><span class="fa fa-check text-success"></span> Save your favorites</li>
                          <li><span class="fa fa-check text-success"></span> Fast checkout</li>
                          <li><span class="fa fa-check text-success"></span> Get a gift <small>(only new customers)</small></li>
                          <li><a href="/read-more/"><u>Read more</u></a></li>
                      </ul>
                      <p><a href="/new-customer/" class="btn btn-info btn-block">Yes please, register now!</a></p>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
