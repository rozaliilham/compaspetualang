    <!-- Trip Terbaru -->
    <section class="tripbaru">
        <div class="col-md-12">
                    <div id="trip-list">
                        <div class="trip-item">
                            <div class="row">
                                <div class="col-md-4 item-media">
                                    <div class="img-responsive">
                                            <img src="<?php echo base_url(); ?>/assets/img/3.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-5 item-body">
                                    <div class="item-title">
                                        <h5>
                                            <a href="#">Menjangan Bali Spectacular</a>
                                        </h5>
                                    </div>
                                    <div class="item-list">
                                        <ul>
                                            <li>4 Atraksi (Diving, Surfing, Barbeque)</li>
                                            <li>2 hari, 1 malam</li>
                                            <li>Menjangan, Kuta, Tanah Lot</li>
                                        </ul>
                                    </div>
                                    <div class="item-footer">
                                        <hr>
                                        <div class="item-icon">
                                            <p>Periode Booking : 12 - 16 Februari 2016 <strong style="color:#f39c12;">(<i class="fa fa-check"></i> Tersedia)</strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 item-price-booking">
                                    <div class="price">
                                        <b><i>2 - 4 Desember 2015</i></b>
                                        <br>
                                        <p><i class="fa fa-map-marker"></i> Poin Meeting : Surabaya</p>
                                        <h4>IDR 2.000.000 <small>/org</small></h4>
                                    </div>
                                    <a href="<?php echo site_url('checkout'); ?>" class="awe-btn" role="button">Pesan Sekarang <i class="fa fa-angle-double-right fa-lg"></i></a>
                                </div>
                            </div>
                                
                        </div>
                        <div class="trip-item">
                            <div class="row">
                                <div class="col-md-4 item-media">
                                    <div class="img-responsive">
                                            <img src="<?php echo base_url(); ?>/assets/img/3.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-md-5 item-body">
                                    <div class="item-title">
                                        <h5>
                                            <a href="#">Menjangan Bali Spectacular</a> 
                                            
                                        </h5>
                                    </div>
                                    <div class="item-list">
                                        <ul>
                                            <li> </li>
                                            <li>4 Atraksi (Diving, Surfing, Barbeque)</li>
                                            <li>2 hari, 1 malam [Menjangan, Kuta, Tanah Lot]</li>
                                        </ul>
                                    </div>
                                    <div class="item-footer">
                                        <hr>
                                        <div class="item-icon">
                                        <p>Periode Booking : 12 - 16 Januari 2016 <strong style="color:#e74c3c;">(<i class="fa fa-times"></i> Ditutup)</strong></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 item-price-booking">
                                    <div class="price">
                                        <b><i>2 - 4 Desember 2015</i></b>
                                        <br>
                                        <p><i class="fa fa-map-marker"></i> Poin Meeting : Surabaya</p>
                                        <h4>IDR 2.000.000 <small>/org</small></h4>
                                    </div>
                                    <button href="#s" class="awe-btn" role="button" style="background:#e74c3c;color:#ecf0f1;" disabled><i class="fa fa-times"></i> Ditutup</button>
                                </div>
                            </div>
                                
                        </div>
                        
                    </div>
                <div class="col-md-12" align="center" style="margin-top:10px;">
                    <button type="button" class="btn btn-warning tipe1">Lihat Semua Trip</button>
                </div>
        </div>
    </section>

    <!-- /Trip Terbaru -->